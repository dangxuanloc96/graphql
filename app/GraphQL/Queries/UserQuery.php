<?php

namespace App\GraphQL\Queries;
use App\Product;
use App\User;
use Illuminate\Support\Facades\App;

class UserQuery
{
    public function all($id)
    {
        return User::where('id', $id)->get();
    }

    public function find($root, $args)
    {
        $email = $args['email'];
        return User::where('email', $email)->first();
    }
}
