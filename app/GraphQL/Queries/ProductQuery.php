<?php

namespace App\GraphQL\Queries;
use App\Product;
use App\User;
use Illuminate\Support\Facades\App;

class ProductQuery
{
    public function all()
    {
        return Product::all();
    }

    public function find($root, $arg)
    {
        return Product::where('type', $arg['type'])->get();
    }
}
