<?php
namespace App\GraphQL\Types;
use App\User;

Class UserType
    {
        public function jobs(User $user)
        {
            return $user->all();
        }
    }
?>
