<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';

    protected $fillable =  [
        'name',
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsToMany( User::class, RoleUser::class, 'user_id', 'role_id' );
    }
}
