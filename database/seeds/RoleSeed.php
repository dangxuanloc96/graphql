<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listRole = [];
        $list = ['BA', 'QA', 'DEV', 'PM', 'Tester'];
        $faker =  $faker = Faker\Factory::create('vi_VN');
        for ($i = 0; $i < 5; $i++) {
            $role = [];
            $role['name'] = $list[$i];
            $listRole[] = $role;
        }
        DB::table('Role')->insert($listRole);
    }
}
