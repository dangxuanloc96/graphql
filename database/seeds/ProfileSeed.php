<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profiles = [];
        $faker =  $faker = Faker\Factory::create('vi_VN');
        for ($i = 0; $i < 50; $i++) {
            $profile = [];
            $profile['user_id'] = $i+1;
            $profile['address'] = $faker->address;
            $profile['phone'] = $faker->phoneNumber;
            $profile['certificate'] = $faker->company;
            $profiles [] = $profile;
        }
        DB::table('profile')->insert($profiles);
    }
}


