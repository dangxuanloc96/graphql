<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [];
        $faker =  $faker = Faker\Factory::create('vi_VN');
        for ($i = 0; $i < 50; $i++) {
            $user['name'] = $faker->name;
            $user['email'] = $faker->email;
            $user['password'] = bcrypt('11111111');
            $users [] = $user;
        }
        DB::table('users')->insert($users);
    }
}
