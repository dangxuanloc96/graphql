<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [];
        $faker =  $faker = Faker\Factory::create('vi_VN');
        for ($i = 0; $i < 50; $i++) {
            $product = [];
            $product['name'] = $faker->firstName;
            $product['type'] = $faker->text;
            $product['quantity'] = $faker->numberBetween(10, 100);
            $products [] = $product;
        }
        DB::table('product')->insert($products);
    }
}
